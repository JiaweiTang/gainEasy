export type Unit = 'dolar' | 'coin';

export interface PriceFrontModelFront {
  symbol: string;
  spotPrice: number;
  futurePrice: number;
  futureSpotDiff: number;
  performanceEndFuture: number;
  timeToDelivery: number;
  performanceYear: number;
  unit: Unit;
}

export interface FundingRateFront {
  symbol: string;
  lastRate: number;
  avgLastDayRate: number;
  avgLastSevenDaysRate: number;
  avgLastThirtyDaysRate: number;
  avgLastSixtyDaysRate: number;
  avgLastThounsandRate: number;
  performanceYearLastRate: number;
  performanceYearAvgLastDayRate: number;
  performanceYearAvgLastSevenDaysRate: number;
  performanceYearAvgLastThirtyDaysRate: number;
  performanceYearAvgLastSixtyDaysRate: number;
  performanceYearAvgLastThounsandRate: number;
  lastThounsandRateSize: number;
}

export interface FundingRateFrontAll extends FundingRateFront {
  fundingRateDetail: FundingRateDataDetail[];
}

export interface FundingRateDataDetail {
  symbol: string;
  fundingTime: moment.Moment;
  fundingRate: number;
}
