import { Box, Button, CircularProgress, Grid } from '@material-ui/core';
import Container from '@material-ui/core/Container';
import Paper from '@material-ui/core/Paper';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import Tab from '@material-ui/core/Tab';
import Tabs from '@material-ui/core/Tabs';
import AllInclusiveIcon from '@material-ui/icons/AllInclusive';
import CopyrightIcon from '@material-ui/icons/Copyright';
import MonetizationOnIcon from '@material-ui/icons/MonetizationOn';
import React, { useEffect, useState } from 'react';
import CardAppCoin from './Card/CardAppCoin';
import CardAppUsd from './Card/CardAppUsd';
//import datas from './data.json'
import { FundingRateFrontAll, PriceFrontModelFront } from './modelFront';
import { fetchFundingRateHistoryBySymbol, fetchFutureCoinMarketData } from './Services/Calls';
import { calculateCoinGainData, calculatePerpetuaData, calculateUsdGainData } from './Services/Service';
import TablePerpetua from './Table/pepetua/TablePerpetua';
import EnhancedTable from './Table/Table';
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    container: {
      paddingBottom: theme.spacing(2),
    },
  }),
);

const REFRESH_TIMES_CONST = [1000, 2000, 3000, 4000, 5000];

const Main = (): JSX.Element => {
  const classes = useStyles();
  const [gainUsdDatas, setGainUsdDatas] = useState<PriceFrontModelFront[]>([]);
  const [gainCoinDatas, setGainCoinDatas] = useState<PriceFrontModelFront[]>([]);
  const [perpetuaData, setPerpetuaData] = useState<FundingRateFrontAll[]>([]);

  const [selectedUsdData, setSelectedUsdData] = useState<string>('');
  const [selectedCoinData, setSelectedCoinData] = useState<string>('');
  const [selectedPerpetuaData, setSelectedPerpetuaData] = useState<string>('');
  const [isLoadingPerpetua, setIsLoadingPerpetua] = useState(false);

  const [tabIndex, setTabIndex] = useState<number>(0);
  const [refreshMilis, setRefreshMilis] = useState<number>(REFRESH_TIMES_CONST[1]);
  const [intervalAPP, setIntervalAPP] = useState<NodeJS.Timeout>();
  const [accountIsConnected, setAccountIsConnected] = useState(false);

  const fetchDatas = () => {
    fetchFutureCoinMarketData().then((data) => {
      const withoutPerpetua = data.filter((d) => d.nextFundingTime == 0);
      setGainUsdDatas(
        withoutPerpetua
          .filter((d) => parseFloat(d.markPrice) - parseFloat(d.indexPrice) >= 0)
          .map((d) => calculateUsdGainData(d)),
      );

      setGainCoinDatas(
        withoutPerpetua
          .filter((d) => parseFloat(d.markPrice) - parseFloat(d.indexPrice) < 0)
          .map((d) => calculateCoinGainData(d)),
      );
    });
  };

  const fetchFundingRateHistory = async () => {
    try {
      setIsLoadingPerpetua(true);
      const futureAlldata = await fetchFutureCoinMarketData();
      const onlyPerpetua = futureAlldata.filter((d) => d.nextFundingTime !== 0);

      const perpetuaDataSetter: FundingRateFrontAll[] = [];

      for (const p of onlyPerpetua) {
        const fundingRatesBack = await fetchFundingRateHistoryBySymbol(p.symbol);
        perpetuaDataSetter.push(calculatePerpetuaData(fundingRatesBack));
      }
      setPerpetuaData(perpetuaDataSetter);
      setIsLoadingPerpetua(false);
    } catch (error) {
      console.error(error);
    }
  };

  useEffect(() => {
    fetchDatas();
    fetchFundingRateHistory();
    setIntervalAPP(
      setInterval(() => {
        fetchDatas();
      }, refreshMilis),
    );

    if (intervalAPP) {
      return () => clearInterval(intervalAPP);
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    if (intervalAPP) {
      clearInterval(intervalAPP);

      setIntervalAPP(
        setInterval(() => {
          fetchDatas();
        }, refreshMilis),
      );
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [refreshMilis]);

  const handleChange = (event: any, newValue: number) => {
    setTabIndex(newValue);
  };

  return (
    <Container maxWidth={false} className={classes.container}>
      <Paper square>
        <Tabs
          value={tabIndex}
          onChange={handleChange}
          variant="scrollable"
          indicatorColor="secondary"
          textColor="secondary"
          aria-label="icon label tabs example"
        >
          <Tab icon={<MonetizationOnIcon />} label={`GAIN USD [${gainUsdDatas.length}]`} />
          <Tab icon={<CopyrightIcon />} label={`GAIN COINS [${gainCoinDatas.length}]`} />
          <Tab
            icon={isLoadingPerpetua ? <CircularProgress size={20} /> : <AllInclusiveIcon />}
            label={`PERPETUA  [${perpetuaData.length}]`}
          />
        </Tabs>
      </Paper>

      {tabIndex == 0 && (
        <Grid container direction="row" spacing={1}>
          <Grid item xs>
            <EnhancedTable
              refreshTime={refreshMilis}
              refreshTimesConst={REFRESH_TIMES_CONST}
              onClickRow={(symbol: string) => setSelectedUsdData(symbol)}
              onClickRefreshTime={(time: number) => setRefreshMilis(time)}
              rows={gainUsdDatas}
            />
          </Grid>
          <Grid item xs={12} lg={4} style={{ paddingTop: '40px' }}>
            <div style={{ position: 'sticky', top: '38px' }}>
              <CardAppUsd selectedData={gainUsdDatas.find((p) => p.symbol === selectedUsdData)} />
              {!accountIsConnected && (
                <Button style={{ marginTop: '8px' }} variant="outlined" fullWidth color="primary">
                  Account Connect
                </Button>
              )}
            </div>
          </Grid>
        </Grid>
      )}
      {tabIndex == 1 && (
        <Grid container direction="row" spacing={1}>
          <Grid item xs>
            <EnhancedTable
              refreshTime={refreshMilis}
              refreshTimesConst={REFRESH_TIMES_CONST}
              onClickRow={(symbol: string) => setSelectedCoinData(symbol)}
              onClickRefreshTime={(time: number) => setRefreshMilis(time)}
              rows={gainCoinDatas}
            />
          </Grid>
          <Grid item xs={12} lg={4} style={{ paddingTop: '40px' }}>
            <CardAppCoin selectedData={gainCoinDatas.find((p) => p.symbol === selectedCoinData)} />
          </Grid>
        </Grid>
      )}

      {tabIndex == 2 && (
        <Grid container direction="row" spacing={1}>
          {isLoadingPerpetua ? (
            <Grid container item xs justify="center" alignItems="center">
              <Box height="100%" m={20}>
                <CircularProgress />
              </Box>
            </Grid>
          ) : (
            <>
              <Grid item xs>
                <TablePerpetua
                  onClickRow={(symbol: string) => setSelectedPerpetuaData(symbol)}
                  onClickRefresh={() => fetchFundingRateHistory()}
                  rows={perpetuaData}
                />
              </Grid>
            </>
          )}
        </Grid>
      )}
    </Container>
  );
};

export default Main;
