import { Card, CardContent, CardHeader, Grid, TextField } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import React, { useState } from 'react';
import { PriceFrontModelFront } from '../modelFront';

interface CardAppProps {
  selectedData: PriceFrontModelFront | undefined;
}

const CardAppUsd = ({ selectedData }: CardAppProps): JSX.Element => {
  const [initialCap, setInitialCap] = useState(1000);
  const [contractSize, setContractSize] = useState(100);
  return (
    <Card>
      {selectedData ? (
        <React.Fragment>
          <CardHeader
            title={selectedData.symbol + ` (${(selectedData.performanceYear * 100).toFixed(2)}%)`}
            subheader={` Time to delivery ${selectedData.timeToDelivery} (${(
              selectedData.performanceEndFuture * 100
            ).toFixed(2)} %) `}
          ></CardHeader>
          <CardContent>
            <Grid container direction={'column'} spacing={2}>
              <Grid container item spacing={2}>
                <Grid item xs={6}>
                  <TextField
                    value={initialCap}
                    fullWidth
                    onChange={(event) => setInitialCap(parseInt(event.target.value) || 0.0)}
                    type="number"
                    label="Inicial Capital ($)"
                    variant="outlined"
                  />
                </Grid>
                <Grid item xs={6}>
                  <TextField
                    value={contractSize}
                    fullWidth
                    onChange={(event) => setContractSize(parseInt(event.target.value) || 0.0)}
                    type="number"
                    label="Size of contract"
                    variant="outlined"
                  />
                </Grid>
              </Grid>
              <Grid container item direction={'column'}>
                <Grid item>
                  <Typography variant="subtitle1">{` Performance ${selectedData.timeToDelivery} days (+${(
                    initialCap * selectedData.performanceEndFuture
                  ).toFixed(2)}$)`}</Typography>
                </Grid>
                <Grid item>
                  <Typography variant="subtitle2">{`${(
                    initialCap * selectedData.performanceEndFuture +
                    initialCap
                  ).toFixed(2)} $`}</Typography>
                </Grid>
              </Grid>

              <Grid container item direction={'column'}>
                <Grid item>
                  <Typography variant="subtitle1">{` Performance year (+${(
                    initialCap * selectedData.performanceYear
                  ).toFixed(2)}$)`}</Typography>
                </Grid>
                <Grid item>
                  <Typography variant="subtitle1">{`${(initialCap * selectedData.performanceYear + initialCap).toFixed(
                    2,
                  )} $`}</Typography>
                </Grid>
              </Grid>

              <Grid container item direction={'column'}>
                <Grid item>
                  <Typography variant="subtitle1">{`Amount of contract to Sell`}</Typography>
                </Grid>
                <Grid item>
                  <Typography variant="subtitle1">{`${Math.floor(
                    (initialCap * selectedData.performanceEndFuture + initialCap) / contractSize,
                  ).toFixed(0)}`}</Typography>
                </Grid>
              </Grid>
            </Grid>
          </CardContent>
        </React.Fragment>
      ) : (
        <CardHeader title="Calculadora" subheader="Seleciona uno"></CardHeader>
      )}
    </Card>
  );
};

export default CardAppUsd;
