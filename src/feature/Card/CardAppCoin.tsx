import { Card, CardContent, CardHeader, Divider, Grid, TextField } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import React, { useState } from 'react';
import { PriceFrontModelFront } from '../modelFront';
import { getUnit } from '../Services/Service';

interface CardAppProps {
  selectedData: PriceFrontModelFront | undefined;
}

const CardAppCoin = ({ selectedData }: CardAppProps): JSX.Element => {
  const [initialCap, setInitialCap] = useState(0.5);
  const [contractSize, setContractSize] = useState(100);

  return (
    <Card style={{ position: 'sticky', top: '38px' }}>
      {selectedData ? (
        <React.Fragment>
          <CardHeader
            title={selectedData.symbol + ` (${(selectedData.performanceYear * 100).toFixed(8)}%)`}
            subheader={` Time to delivery ${selectedData.timeToDelivery} (${(
              selectedData.performanceEndFuture * 100
            ).toFixed(8)} %) `}
          ></CardHeader>
          <CardContent>
            <Grid container direction={'column'} spacing={2}>
              <Grid container item spacing={2}>
                <Grid item xs={6}>
                  <TextField
                    value={initialCap}
                    fullWidth
                    onChange={(event) => setInitialCap(parseFloat(event.target.value) || 0.0)}
                    type="number"
                    label={`Inicial Capital (${getUnit(selectedData)})`}
                    variant="outlined"
                  />
                </Grid>
                <Grid item xs={6}>
                  <TextField
                    value={contractSize}
                    fullWidth
                    onChange={(event) => setContractSize(parseInt(event.target.value) || 0.0)}
                    type="number"
                    label="Size of contract"
                    variant="outlined"
                  />
                </Grid>
              </Grid>
              <Grid container item direction={'column'}>
                <Grid item>
                  <Typography variant="subtitle1">{` Performance ${selectedData.timeToDelivery} days (+${(
                    initialCap * selectedData.performanceEndFuture
                  ).toFixed(8)} ${getUnit(selectedData)})`}</Typography>
                </Grid>
                <Grid item>
                  <Typography variant="subtitle2">{`${(
                    initialCap * selectedData.performanceEndFuture +
                    initialCap
                  ).toFixed(8)} ${getUnit(selectedData)}`}</Typography>
                </Grid>
              </Grid>

              <Grid container item direction={'column'}>
                <Grid item>
                  <Typography variant="subtitle1">{` Performance year (+${(
                    initialCap * selectedData.performanceYear
                  ).toFixed(8)} ${getUnit(selectedData)})`}</Typography>
                </Grid>
                <Grid item>
                  <Typography variant="subtitle2">{`${(initialCap * selectedData.performanceYear + initialCap).toFixed(
                    2,
                  )} ${getUnit(selectedData)} `}</Typography>
                </Grid>
              </Grid>

              <Divider></Divider>

              <Grid container item>
                <Typography variant="subtitle1" style={{ fontWeight: 'bold' }}>
                  Steep
                </Typography>
              </Grid>

              <Grid container item direction={'column'}>
                <Grid item>
                  <Typography variant="subtitle1">{`1. Sell ${initialCap}${getUnit(
                    selectedData,
                  )} in spot price ${selectedData.spotPrice.toFixed(0)}$:`}</Typography>
                </Grid>
                <Grid item>
                  <Typography variant="subtitle2">{`${initialCap * selectedData.spotPrice} $`}</Typography>
                </Grid>
              </Grid>

              <Grid container item direction={'column'}>
                <Grid item>
                  <Typography variant="subtitle1">{`2. Change spot to margin`}</Typography>
                </Grid>
              </Grid>

              <Grid container item direction={'column'}>
                <Grid item>
                  <Typography variant="subtitle1">{`3. Long future contract in ${selectedData.futurePrice.toFixed(
                    0,
                  )}$ with: `}</Typography>
                </Grid>
                <Grid item>
                  <Typography variant="subtitle2">{`${Math.floor(
                    (selectedData.spotPrice * initialCap) / contractSize,
                  )} CONTRACT`}</Typography>
                </Grid>
              </Grid>
            </Grid>
          </CardContent>
        </React.Fragment>
      ) : (
        <CardHeader title="Calculadora" subheader="Seleciona uno"></CardHeader>
      )}
    </Card>
  );
};

export default CardAppCoin;
