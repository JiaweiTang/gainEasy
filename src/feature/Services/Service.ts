import { meanBy } from 'lodash';
import moment from 'moment';
import { FundingRateHistoryBack, FuturePriceModelBack } from '../modelBack';
import { FundingRateDataDetail, FundingRateFrontAll, PriceFrontModelFront } from '../modelFront';

/**
 * return the total of capital
 *
 * @param initialCap
 * @interest : exmaple if you have 0.0196%, pass 0.000196
 * @time time to compound, if you whant to reinvent each day pass 365
 *
 */
const _calculateCompoundInterest = (initialCap: number, interest: number, time: number): number => {
  return initialCap * Math.pow(1 + interest, time);
};

const _calculatePerformanceYear = (interest: number, time: number): number => {
  const _compountedInterest: number = _calculateCompoundInterest(100, interest, time);
  return (_compountedInterest - 100) / 100;
};

export const calculateUsdGainData = (fb: FuturePriceModelBack): PriceFrontModelFront => {
  const spotPrice: number = parseFloat(fb.indexPrice);
  const futurePrice: number = parseFloat(fb.markPrice);
  const futureSpotDiff: number = futurePrice - spotPrice;
  const performanceEndFuture: number = futureSpotDiff / spotPrice;
  const _dateExpiration: moment.Moment = moment(fb.symbol.split('_')[1], 'YYMMDD');
  const timeToDelivery: number = _dateExpiration.diff(moment(), 'days');

  return {
    symbol: fb.symbol,
    spotPrice,
    futurePrice,
    futureSpotDiff,
    performanceEndFuture,
    timeToDelivery,
    performanceYear: _calculatePerformanceYear(performanceEndFuture, 365 / timeToDelivery),
    unit: 'dolar',
  };
};

export const calculateCoinGainData = (fb: FuturePriceModelBack): PriceFrontModelFront => {
  const spotPrice: number = parseFloat(fb.indexPrice);
  const futurePrice: number = parseFloat(fb.markPrice);
  const futureSpotDiff: number = spotPrice - futurePrice;
  const performanceEndFuture: number = futureSpotDiff / futurePrice;
  const _dateExpiration: moment.Moment = moment(fb.symbol.split('_')[1], 'YYMMDD');
  const timeToDelivery: number = _dateExpiration.diff(moment(), 'days');

  return {
    symbol: fb.symbol,
    spotPrice,
    futurePrice,
    futureSpotDiff,
    performanceEndFuture,
    timeToDelivery,
    performanceYear: _calculatePerformanceYear(performanceEndFuture, 365 / timeToDelivery),
    unit: 'coin',
  };
};

export const MOMENT_FORMAT = 'DD/MM/YYYY HH:mm';

export const calculatePerpetuaData = (frbs: FundingRateHistoryBack[]): FundingRateFrontAll => {
  const sortedFundingRateAsc: FundingRateDataDetail[] = frbs.map((o) => ({
    symbol: o.symbol,
    fundingTime: moment(o.fundingTime),
    fundingRate: parseFloat(o.fundingRate),
  }));

  //get last rate
  const lastRate = sortedFundingRateAsc.pop();

  const avgLastDayRate = meanBy(
    sortedFundingRateAsc.filter((x) => x.fundingTime.isSame(moment().subtract(1, 'day'), 'day')),
    (o) => o.fundingRate,
  );

  const avgLastSevenDaysRate = meanBy(
    sortedFundingRateAsc.filter((x) => x.fundingTime.isSameOrAfter(moment().subtract(7, 'day'), 'day')),
    (o) => o.fundingRate,
  );
  const avgLastThirtyDaysRate = meanBy(
    sortedFundingRateAsc.filter((x) => x.fundingTime.isSameOrAfter(moment().subtract(30, 'day'), 'day')),
    (o) => o.fundingRate,
  );

  const avgLastSixtyDaysRate = meanBy(
    sortedFundingRateAsc.filter((x) => x.fundingTime.isSameOrAfter(moment().subtract(60, 'day'), 'day')),
    (o) => o.fundingRate,
  );
  const avgLastThounsandRate = meanBy(sortedFundingRateAsc, (o) => o.fundingRate);

  return {
    symbol: lastRate?.symbol as string,
    lastRate: lastRate?.fundingRate as number,
    avgLastDayRate,
    avgLastSevenDaysRate,
    avgLastThirtyDaysRate,
    avgLastSixtyDaysRate,
    avgLastThounsandRate,
    performanceYearLastRate: _calculatePerformanceYear((lastRate?.fundingRate as number) * 3, 365),
    performanceYearAvgLastDayRate: _calculatePerformanceYear(avgLastDayRate * 3, 365),
    performanceYearAvgLastSevenDaysRate: _calculatePerformanceYear(avgLastSevenDaysRate * 3, 365),
    performanceYearAvgLastThirtyDaysRate: _calculatePerformanceYear(avgLastThirtyDaysRate * 3, 365),
    performanceYearAvgLastSixtyDaysRate: _calculatePerformanceYear(avgLastSixtyDaysRate * 3, 365),
    performanceYearAvgLastThounsandRate: _calculatePerformanceYear(avgLastThounsandRate * 3, 365),
    fundingRateDetail: sortedFundingRateAsc,
    lastThounsandRateSize: sortedFundingRateAsc.length,
  };
};

export const getUnit = (p: PriceFrontModelFront): string => {
  return p.symbol.split('_')[0].replace('USD', '');
};
