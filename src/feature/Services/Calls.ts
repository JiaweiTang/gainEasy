import axiosInstance from '../../configuration/Axios';
import { ExchangeInfo, FundingRateHistoryBack, FuturePriceModelBack } from '../modelBack';

const CONTRACT_DELIVERY_URL = 'https://dapi.binance.com/dapi';

export const fetchFutureCoinMarketData = async (): Promise<FuturePriceModelBack[]> => {
  try {
    const response = await axiosInstance.get<FuturePriceModelBack[]>(`${CONTRACT_DELIVERY_URL}/v1/premiumIndex`);
    return response.data;
  } catch (error) {
    return error;
  }
};

export const fetchExchangeInfo = async (): Promise<ExchangeInfo> => {
  try {
    const response = await axiosInstance.get<ExchangeInfo>(`${CONTRACT_DELIVERY_URL}/v1/exchangeInfo`);

    return response.data;
  } catch (error) {
    return error;
  }
};

export const fetchFundingRateHistoryBySymbol = async (symbol: string): Promise<FundingRateHistoryBack[]> => {
  try {
    const response = await axiosInstance.get<FundingRateHistoryBack[]>(`${CONTRACT_DELIVERY_URL}/v1/fundingRate`, {
      params: {
        symbol,
        limit: 1000,
      },
    });

    return response.data;
  } catch (error) {
    return error;
  }
};
