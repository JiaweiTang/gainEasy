import { Grid } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Paper from '@material-ui/core/Paper';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import Switch from '@material-ui/core/Switch';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableRow from '@material-ui/core/TableRow';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { maxBy } from 'lodash';
import React from 'react';
import { FundingRateFront } from '../../modelFront';
import { getComparator, Order, stableSort } from '../TableUtils';
import { TablePerpetuaHead } from './TablePerpetuaHead';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '100%',
    },
    paper: {
      width: '100%',
      marginBottom: theme.spacing(2),
    },
    table: {
      minWidth: 750,
    },
    visuallyHidden: {
      border: 0,
      clip: 'rect(0 0 0 0)',
      height: 1,
      margin: -1,
      overflow: 'hidden',
      padding: 0,
      position: 'absolute',
      top: 20,
      width: 1,
    },
  }),
);

interface EnhancedMainTableProps {
  readonly rows: FundingRateFront[];
  onClickRefresh: () => void;
  onClickRow: (symbol: string) => void;
}

const TablePerpetua = (props: EnhancedMainTableProps): JSX.Element => {
  const classes = useStyles();
  const [order, setOrder] = React.useState<Order>('desc');
  const [orderBy, setOrderBy] = React.useState<keyof FundingRateFront>('avgLastThounsandRate');
  const [selected, setSelected] = React.useState<string>('');
  const [dense, setDense] = React.useState(true);
  const [maxValues, setMaxValues] = React.useState<{
    maxLastRateSymbol?: string;
    maxAvgLastDayRateSymbol?: string;
    maxAvgLastSevenDaysRateSymbol?: string;
    maxAvgLastThirtyDaysRateSymbol?: string;
    maxAvgLastSixtyDaysRateSymbol?: string;
    maxAvgLastThounsandRateSymbol?: string;
  }>({});

  React.useEffect(() => {
    setMaxValues({
      maxLastRateSymbol: maxBy(props.rows, (r) => r.lastRate)?.symbol,
      maxAvgLastDayRateSymbol: maxBy(props.rows, (r) => r.avgLastDayRate)?.symbol,
      maxAvgLastSevenDaysRateSymbol: maxBy(props.rows, (r) => r.avgLastSevenDaysRate)?.symbol,
      maxAvgLastThirtyDaysRateSymbol: maxBy(props.rows, (r) => r.avgLastThirtyDaysRate)?.symbol,
      maxAvgLastSixtyDaysRateSymbol: maxBy(props.rows, (r) => r.avgLastSixtyDaysRate)?.symbol,
      maxAvgLastThounsandRateSymbol: maxBy(props.rows, (r) => r.avgLastThounsandRate)?.symbol,
    });
  }, [props.rows]);

  const handleRequestSort = (event: React.MouseEvent<unknown>, property: keyof FundingRateFront) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  const handleClick = (event: React.MouseEvent<unknown>, name: string) => {
    setSelected(name);
    props.onClickRow(name);
  };

  const handleChangeDense = (event: React.ChangeEvent<HTMLInputElement>) => {
    setDense(event.target.checked);
  };

  const getTableRowColor = (row: FundingRateFront): React.CSSProperties => {
    let color = 'none';
    let fontWeight = 'normal';
    if (row.symbol.includes('BTC')) {
      color = 'RGB(242, 169, 0)';
      fontWeight = 'bold';
    } else if (row.symbol.includes('ETH')) {
      color = 'RGB(25, 26, 26)';
      fontWeight = 'bold';
    }
    return {
      color: color,
      fontWeight: fontWeight as any,
    };
  };

  const isSelected = (name: string) => selected === name;

  return (
    <div className={classes.root}>
      <Grid container justify="space-between">
        <Grid item xs>
          <FormControlLabel control={<Switch checked={dense} onChange={handleChangeDense} />} label="Dense padding" />
        </Grid>
        <Grid item container xs justify="flex-end" spacing={2} alignItems="center">
          <Grid item>
            <Button size={'small'} variant="outlined" color={'primary'} onClick={props.onClickRefresh}>
              Refresh
            </Button>
          </Grid>
        </Grid>
      </Grid>

      <Paper className={classes.paper} variant="elevation">
        <Toolbar>
          <Typography variant="h5" id="tableTitle" component="div">
            BINANCE
          </Typography>
        </Toolbar>
        <TableContainer>
          <Table
            className={classes.table}
            aria-labelledby="tableTitle"
            size={dense ? 'small' : 'medium'}
            aria-label="enhanced table"
          >
            <TablePerpetuaHead classes={classes} order={order} orderBy={orderBy} onRequestSort={handleRequestSort} />
            <TableBody>
              {stableSort(props.rows, getComparator(order, orderBy)).map((row) => {
                const isItemSelected = isSelected(row.symbol);

                return (
                  <TableRow
                    hover
                    onClick={(event) => handleClick(event, row.symbol)}
                    aria-checked={isItemSelected}
                    tabIndex={1}
                    key={row.symbol}
                    selected={isItemSelected}
                  >
                    <TableCell align="right" style={getTableRowColor(row)}>
                      {row.symbol}
                    </TableCell>
                    <TableCell
                      align="right"
                      style={
                        maxValues.maxLastRateSymbol === row.symbol
                          ? { fontWeight: 'bold', color: 'RGB(51,102,255)' }
                          : {}
                      }
                    >{`${(row.lastRate * 100).toFixed(4)} [${(row.performanceYearLastRate * 100).toFixed(
                      4,
                    )}]`}</TableCell>

                    <TableCell
                      align="right"
                      style={
                        maxValues.maxAvgLastDayRateSymbol === row.symbol
                          ? { fontWeight: 'bold', color: 'RGB(51,102,255)' }
                          : {}
                      }
                    >{`${(row.avgLastDayRate * 100).toFixed(4)} [${(row.performanceYearAvgLastDayRate * 100).toFixed(
                      4,
                    )}]`}</TableCell>
                    <TableCell
                      align="right"
                      style={
                        maxValues.maxAvgLastSevenDaysRateSymbol === row.symbol
                          ? { fontWeight: 'bold', color: 'RGB(51,102,255)' }
                          : {}
                      }
                    >{`${(row.avgLastSevenDaysRate * 100).toFixed(4)} [${(
                      row.performanceYearAvgLastSevenDaysRate * 100
                    ).toFixed(4)}]`}</TableCell>
                    <TableCell
                      align="right"
                      style={
                        maxValues.maxAvgLastThirtyDaysRateSymbol === row.symbol
                          ? { fontWeight: 'bold', color: 'RGB(51,102,255)' }
                          : {}
                      }
                    >{`${(row.avgLastThirtyDaysRate * 100).toFixed(4)} [${(
                      row.performanceYearAvgLastThirtyDaysRate * 100
                    ).toFixed(4)}]`}</TableCell>
                    <TableCell
                      align="right"
                      style={
                        maxValues.maxAvgLastSixtyDaysRateSymbol === row.symbol
                          ? { fontWeight: 'bold', color: 'RGB(51,102,255)' }
                          : {}
                      }
                    >{`${(row.avgLastSixtyDaysRate * 100).toFixed(4)} [${(
                      row.performanceYearAvgLastSixtyDaysRate * 100
                    ).toFixed(4)}]`}</TableCell>
                    <TableCell
                      align="right"
                      style={
                        maxValues.maxAvgLastThounsandRateSymbol === row.symbol
                          ? { fontWeight: 'bold', color: 'RGB(51,102,255)' }
                          : {}
                      }
                    >{`${(row.avgLastThounsandRate * 100).toFixed(4)} [${(
                      row.performanceYearAvgLastThounsandRate * 100
                    ).toFixed(4)}]`}</TableCell>

                    <TableCell align="right">{`${row.lastThounsandRateSize} [${(row.lastThounsandRateSize / 3).toFixed(
                      2,
                    )}]`}</TableCell>
                  </TableRow>
                );
              })}
            </TableBody>
          </Table>
        </TableContainer>
      </Paper>
    </div>
  );
};

export default TablePerpetua;
