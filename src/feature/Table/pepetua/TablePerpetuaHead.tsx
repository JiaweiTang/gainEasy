import { createStyles, makeStyles } from '@material-ui/core/styles';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import Typography from '@material-ui/core/Typography';
import React from 'react';
import { FundingRateFront } from '../../modelFront';
import { Order } from '../TableUtils';

const useStyles = makeStyles(() =>
  createStyles({
    visuallyHidden: {
      border: 0,
      clip: 'rect(0 0 0 0)',
      height: 1,
      margin: -1,
      overflow: 'hidden',
      padding: 0,
      position: 'absolute',
      top: 20,
      width: 1,
    },
  }),
);

export interface HeadCell {
  disablePadding: boolean;
  id: keyof FundingRateFront;
  label: string;
  numeric: boolean;
}

export const headCells: HeadCell[] = [
  { id: 'symbol', numeric: true, disablePadding: false, label: 'Symbol ' },
  { id: 'lastRate', numeric: true, disablePadding: false, label: 'last rate (%) [PY %]' },
  { id: 'avgLastDayRate', numeric: true, disablePadding: false, label: 'Avg last day Rate (%) [PY %]' },
  { id: 'avgLastSevenDaysRate', numeric: true, disablePadding: false, label: 'Avg last 7 day Rate (%) [PY %]' },
  { id: 'avgLastThirtyDaysRate', numeric: true, disablePadding: false, label: 'Avg last 30 day Rate (%) [PY %]' },
  { id: 'avgLastSixtyDaysRate', numeric: true, disablePadding: false, label: 'Avg last 60 day Rate (%) [PY %]' },
  { id: 'avgLastThounsandRate', numeric: true, disablePadding: false, label: 'Avg last 1000 Rate (%) [PY %]' },
  { id: 'lastThounsandRateSize', numeric: true, disablePadding: false, label: 'Last 1000 Rate size [days]' },
];

export interface EnhancedTableProps {
  classes: ReturnType<typeof useStyles>;
  onRequestSort: (event: React.MouseEvent<unknown>, property: keyof FundingRateFront) => void;
  order: Order;
  orderBy: string;
}

export function TablePerpetuaHead(props: EnhancedTableProps): JSX.Element {
  const { classes, order, orderBy, onRequestSort } = props;
  const createSortHandler = (property: keyof FundingRateFront) => (event: React.MouseEvent<unknown>) => {
    onRequestSort(event, property);
  };

  return (
    <TableHead>
      <TableRow>
        {headCells.map((headCell) => (
          <TableCell
            key={headCell.id}
            align={headCell.numeric ? 'right' : 'left'}
            padding={headCell.disablePadding ? 'none' : 'default'}
            sortDirection={orderBy === headCell.id ? order : false}
          >
            <TableSortLabel
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : 'asc'}
              onClick={createSortHandler(headCell.id)}
            >
              <Typography variant="body1">{headCell.label}</Typography>

              {orderBy === headCell.id ? (
                <span className={classes.visuallyHidden}>
                  {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                </span>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
}
