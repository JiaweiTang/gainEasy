import { Button, ButtonGroup, Grid } from '@material-ui/core';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Paper from '@material-ui/core/Paper';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import Switch from '@material-ui/core/Switch';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableRow from '@material-ui/core/TableRow';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import React from 'react';
import { PriceFrontModelFront } from '../modelFront';
import { EnhancedTableHead } from './TableHead';
import { getComparator, Order, stableSort } from './TableUtils';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '100%',
    },
    paper: {
      width: '100%',
      marginBottom: theme.spacing(2),
    },
    table: {
      minWidth: 750,
    },
    visuallyHidden: {
      border: 0,
      clip: 'rect(0 0 0 0)',
      height: 1,
      margin: -1,
      overflow: 'hidden',
      padding: 0,
      position: 'absolute',
      top: 20,
      width: 1,
    },
  }),
);

interface EnhancedMainTableProps {
  readonly rows: PriceFrontModelFront[];
  readonly refreshTime: number;
  readonly refreshTimesConst: number[];
  onClickRefreshTime: (time: number) => void;
  onClickRow: (symbol: string) => void;
}

const EnhancedTable = (props: EnhancedMainTableProps): JSX.Element => {
  const classes = useStyles();
  const [order, setOrder] = React.useState<Order>('desc');
  const [orderBy, setOrderBy] = React.useState<keyof PriceFrontModelFront>('performanceYear');
  const [selected, setSelected] = React.useState<string>('');
  const [dense, setDense] = React.useState(true);

  const handleRequestSort = (event: React.MouseEvent<unknown>, property: keyof PriceFrontModelFront) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
  };

  const handleClick = (event: React.MouseEvent<unknown>, name: string) => {
    setSelected(name);
    props.onClickRow(name);
  };

  const handleChangeDense = (event: React.ChangeEvent<HTMLInputElement>) => {
    setDense(event.target.checked);
  };

  const getTableRowColor = (row: PriceFrontModelFront): React.CSSProperties => {
    let color = 'none';
    let fontWeight = 'normal';
    if (row.symbol.includes('BTC')) {
      color = 'RGB(242, 169, 0)';
      fontWeight = 'bold';
    } else if (row.symbol.includes('ETH')) {
      color = 'RGB(25, 26, 26)';
      fontWeight = 'bold';
    }
    return {
      color: color,
      fontWeight: fontWeight as any,
    };
  };

  const isSelected = (name: string) => selected === name;

  return (
    <div className={classes.root}>
      <Grid container justify="space-between">
        <Grid item xs>
          <FormControlLabel control={<Switch checked={dense} onChange={handleChangeDense} />} label="Dense padding" />
        </Grid>
        <Grid item container xs justify="flex-end" spacing={2} alignItems="center">
          <Grid item>
            <Typography variant={'body1'}>Refresh time sec</Typography>
          </Grid>
          <Grid item>
            <ButtonGroup size="small" aria-label="small outlined button group">
              {props.refreshTimesConst.map((rtc) =>
                rtc == props.refreshTime ? (
                  <Button key={rtc} variant="contained" color={'primary'} onClick={() => props.onClickRefreshTime(rtc)}>
                    {rtc / 1000}
                  </Button>
                ) : (
                  <Button key={rtc} onClick={() => props.onClickRefreshTime(rtc)}>
                    {rtc / 1000}
                  </Button>
                ),
              )}
            </ButtonGroup>
          </Grid>
        </Grid>
      </Grid>

      <Paper className={classes.paper} variant="elevation">
        <Toolbar>
          <Typography variant="h5" id="tableTitle" component="div">
            BINANCE
          </Typography>
        </Toolbar>
        <TableContainer>
          <Table
            className={classes.table}
            aria-labelledby="tableTitle"
            size={dense ? 'small' : 'medium'}
            aria-label="enhanced table"
          >
            <EnhancedTableHead classes={classes} order={order} orderBy={orderBy} onRequestSort={handleRequestSort} />
            <TableBody>
              {stableSort(props.rows, getComparator(order, orderBy)).map((row) => {
                const isItemSelected = isSelected(row.symbol);

                return (
                  <TableRow
                    hover
                    onClick={(event) => handleClick(event, row.symbol)}
                    aria-checked={isItemSelected}
                    tabIndex={1}
                    key={row.symbol}
                    selected={isItemSelected}
                  >
                    <TableCell align="right" style={getTableRowColor(row)}>
                      {row.symbol}
                    </TableCell>
                    <TableCell align="right">{(row.performanceYear * 100).toFixed(2)}</TableCell>
                    <TableCell align="right">{row.timeToDelivery}</TableCell>
                    <TableCell align="right">{row.spotPrice.toFixed(2)}</TableCell>
                    <TableCell align="right">{row.futurePrice.toFixed(2)}</TableCell>
                    <TableCell align="right">{row.futureSpotDiff.toFixed(4)}</TableCell>
                    <TableCell align="right">{(row.performanceEndFuture * 100).toFixed(2)}</TableCell>
                  </TableRow>
                );
              })}
            </TableBody>
          </Table>
        </TableContainer>
      </Paper>
    </div>
  );
};

export default EnhancedTable;
