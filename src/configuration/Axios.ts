import axios from 'axios';
import { get } from 'lodash';
import { Slide, toast } from 'react-toastify';
const axiosInstance = axios.create();

const isHandlerEnabled = (config = {} as any) => {
  return config.hasOwnProperty('handlerEnabled') && !config.handlerEnabled ? false : true;
};

const requestHandler = (request: any) => {
  if (isHandlerEnabled(request)) {
    //request.headers['Authorization'] = localStorage.getItem('BEARER_TOKEN');
    request.headers['Content-Type'] = 'application/json';
  }
  return request;
};

const errorHandler = (error: any) => {
  if (isHandlerEnabled(error.config)) {
    toast.dark(get(error, 'response.data.error') || 'unknow errer happen', {
      position: 'top-center',
      autoClose: 4000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
      transition: Slide,
    });
  }
  return Promise.reject({ ...error });
};

const successHandler = (response: any) => {
  if (isHandlerEnabled(response.config)) {
    // Handle responses
  }
  return response;
};

axiosInstance.interceptors.request.use((request) => requestHandler(request));

axiosInstance.interceptors.response.use(
  (response) => successHandler(response),
  (error) => errorHandler(error),
);

export default axiosInstance;
