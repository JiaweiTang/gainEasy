import { createMuiTheme } from '@material-ui/core/styles';

const BACKGROUND_COLOR = 'rgb(243,244,246)';

const theme = createMuiTheme({
  palette: {
    primary: {
      light: 'rgba(132, 169, 255, 1)',
      main: 'rgba(51, 102, 255, 1)',
      dark: 'rgb(37, 78, 219, 1)',
    },
    secondary: {
      light: 'rgba(132, 169, 255, 1)',
      main: 'rgba(51, 102, 255, 1)',
      dark: 'rgb(37, 78, 219, 1)',
    },
    success: {
      light: '#CDF477',
      main: '#71D827',
      dark: '#75BE18',
    },
    info: {
      light: '#7EE2FF',
      main: '#46A1FC',
      dark: '#1469B7',
    },
    warning: {
      light: '#FFEC69',
      main: '#FFC005',
      dark: '#B79602',
    },
    error: {
      light: '#FFB582',
      main: '#FF4423',
      dark: '#B72C18',
    },
    background: {
      default: BACKGROUND_COLOR,
    },
    action: {
      hoverOpacity: 0.15,
      disabled: 'rgba(143, 155, 179, 0.48)',
      disabledBackground: '	rgba(143, 155, 179, 0.24)',
      disabledOpacity: 0.5,
    },
    text: {
      primary: 'rgba(34, 43, 69, 1)',
      secondary: 'rgba(34, 43, 69, 0.6)',
      disabled: 'rgba(143, 155, 179, 0.48)',
      hint: '#8f9bb3',
    },
  },
  typography: {
    fontFamily: 'Roboto,Open Sans,sans-serif, Noto Sans SC, Noto Sans JP',
    h1: {
      fontSize: '2.25rem',
      fontWeight: 500,
    },
    h2: {
      fontSize: '2rem',
      fontWeight: 500,
    },
    h3: {
      fontSize: '1.875rem',
      fontWeight: 500,
    },
    h4: {
      fontSize: '1.625rem',
      fontWeight: 500,
    },
    h5: {
      fontSize: '1.375rem',
      fontWeight: 500,
    },
    h6: {
      fontSize: '1.125rem',
      fontWeight: 500,
    },
    subtitle1: {
      fontSize: '0.9375rem',
      fontWeight: 500,
    },
    subtitle2: {
      fontSize: '0.8125rem',
      fontWeight: 500,
    },
    body1: {
      fontSize: '0.9rem',
      fontWeight: 400,
      lineHeight: 1.25,
    },
    body2: {
      fontSize: '0.8125rem',
      fontWeight: 400,
      lineHeight: 1.125,
    },
    button: {
      fontSize: '	0.875rem',
      fontWeight: 500,
    },
    caption: {
      fontSize: '	0.75rem',
      fontWeight: 400,
    },
    overline: {
      fontSize: '	0.75rem',
      fontWeight: 500,
    },
  },
  shape: {
    borderRadius: 8,
  },
  overrides: {
    MuiCard: {
      root: {
        background: 'white',
        boxShadow: '0px 1px 2px rgba(50, 50, 71, 0.08)',
        border: 'none',
      },
    },
    MuiListItem: {
      root: {
        minHeight: '60px',
      },
    },
    MuiBottomNavigation: {
      root: {
        boxShadow: '0px 0px 9px rgba(0, 0, 0, 0.09)',
      },
    },
    MuiDialog: {
      paperFullScreen: {
        background: BACKGROUND_COLOR,
      },
    },
    MuiStepper: {
      root: {
        padding: 0,
      },
    },
  },
});

export default theme;
